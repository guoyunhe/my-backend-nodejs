### Nike Interview Backend Code Challenge [NodeJS]

_The provided code document should contain more details._

Start server (Port 8081): `npm install -> npm start`

APIs:


1. Clone [this project](https://gitlab.com/hiring_nike_china/nike-shoes-backend-nodejs) and start this nodejs server, following the instructions on this project. The project should run at port 9090
2. Get original price (randomly fetched) for a supplied shoe id:
```
URL (GET) - http://localhost:8081/api/shoe-price/1

Response:
{
    "shoePrice": 147
}
```
